package Aviacompany;

import javafx.geometry.Point2D;

/**
 * describe general aircraft
 * @author Igor Benzel
 * @version 1.1 from 23.09.18
 */
public abstract class Aircraft {
    private String engineType;
    private int engineQuantity;
    private int pilotQuantity;

    public Aircraft(String engineType, int engineQuantity, int pilotQuantity) {
        this.engineType = engineType;
        this.engineQuantity = engineQuantity;
        this.pilotQuantity = pilotQuantity;
    }

    public String getEngineType() {
        return engineType;
    }

    public int getEngineQuantity() {
        return engineQuantity;
    }

    public int getPilotQuantity() {
        return pilotQuantity;
    }

    abstract void rise ();

    abstract void fly ();

    abstract void land ();

    public void move (Point2D startPoint, Point2D endPoint) {
        rise ();
        fly ();
        land();
    }
}
