package Aviacompany;

import java.util.ArrayList;

public class MilitaryAircraft extends Aircraft {
    ArrayList <String> veapons;

    public MilitaryAircraft(String engineType, int engineQuantity, int pilotQuantity) {
        super(engineType, engineQuantity, pilotQuantity);
        this.veapons = new ArrayList<String>();
    }

    void rise() {
        System.out.println("Aircraft rise!");
    }

    void fly() {
        System.out.println("Aircraft fly!");
    }

    void land() {
        System.out.println("Aircraft land!");
    }

    public ArrayList<String> getVeapons() {
        return veapons;
    }

    public String addVeapon (String veapon){
        System.out.println("Added veapon " + veapon);
        return veapon;
    }
}
