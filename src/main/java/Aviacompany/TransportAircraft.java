package Aviacompany;

public class TransportAircraft extends Aircraft implements AdditionalDoor, DurableChassis {
    private String model;
    private int serialNumber;
    private int cargoVolume;
    private int cargoWeight;
    private int speed;
    private int maxDistance;
    private int fuelUsage;
    private boolean additionalDoor = false;
    private boolean additionalVeels = false;

    public TransportAircraft(String engineType, int engineQuantity, int pilotQuantity, String model) {
        super(engineType, engineQuantity, pilotQuantity);
        this.model = model;
    }

    void rise() {
        System.out.println("Aircraft rise!");
    }

    void fly() {
        System.out.println("Aircraft fly!");
    }

    void land() {
        System.out.println("Aircraft land!");
    }

    public int getCargoVolume() {
        return cargoVolume;
    }

    public void setCargoVolume(int cargoVolume) {
        this.cargoVolume = cargoVolume;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(int cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    public int getFuelUsage() {
        return fuelUsage;
    }

    public void setFuelUsage(int fuelUsage) {
        this.fuelUsage = fuelUsage;
    }

    @Override
    public String toString() {
        return "TransportAircraft{" +
                "model='" + model + '\'' +
                ", cargoVolume=" + cargoVolume +
                ", cargoWeight=" + cargoWeight +
                ", maxDistance=" + maxDistance +
                ", fuelUsage=" + fuelUsage +
                '}';
    }

    public void addDoor(String doorType) {
        additionalDoor = true;
        System.out.println("Additional door can be implemented!");
    }

    public void addVeels(int veelsNum) {
        additionalVeels = true;
        System.out.println("Additional veels can be implemented!");
    }
}
