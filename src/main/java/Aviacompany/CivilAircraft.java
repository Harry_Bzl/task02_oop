package Aviacompany;

class CivilAircraft extends Aircraft {
    private int passangerNumber;

    public CivilAircraft(String engineType, int engineQuantity, int pilotQuantity, int passangerNumber) {
        super(engineType, engineQuantity, pilotQuantity);
        this.passangerNumber = passangerNumber;
    }

    void rise() {
        System.out.println("Aircraft rise!");
    }

    void fly() {
        System.out.println("Aircraft fly!");
    }

    void land() {
        System.out.println("Aircraft land!");
    }

    public int getPassangerNumber() {
        return passangerNumber;
    }

    public void setPassangerNumber(int passangerNumber) {
        this.passangerNumber = passangerNumber;
    }
}
