package Aviacompany;

public class CivilTransportAircraft extends TransportAircraft implements Modifyable {
    public CivilTransportAircraft(String engineType, int engineQuantity, int pilotQuantity, String model) {
        super(engineType, engineQuantity, pilotQuantity, model);
    }

    public void modify() {
        System.out.println("Aircraft vas modifyed.");
    }
}
