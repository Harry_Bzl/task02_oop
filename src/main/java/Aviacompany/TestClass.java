package Aviacompany;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * for testing aviacompany
 * @author Igor Benzel
 * @version 1.1 from 23.09.18
 */
public class TestClass {
    public static void main(String[] args) {
        AviatransportCompany company = new AviatransportCompany();
        TransportAircraft aircraft1 = new TransportAircraft("reactive",
                2, 2, "Airbus-a30");
        aircraft1.setCargoVolume(116);
        aircraft1.setCargoWeight(48000);
        aircraft1.setFuelUsage(4000);
        aircraft1.setMaxDistance(7400);
        company.addAircraft(aircraft1);

        TransportAircraft aircraft2 = new TransportAircraft("reactive",
                4, 2, "Airbus-Beluga");
        aircraft2.setCargoVolume(1210);
        aircraft2.setCargoWeight(47000);
        aircraft2.setFuelUsage(3500);
        aircraft2.setMaxDistance(4632);
        company.addAircraft(aircraft2);

        TransportAircraft aircraft3 = new TransportAircraft("reactive",
                4, 2, "Antonov An-124");
        aircraft3.setCargoVolume(1028);
        aircraft3.setCargoWeight(15000);
        aircraft3.setFuelUsage(4200);
        aircraft3.setMaxDistance(5400);
        company.addAircraft(aircraft3);

        TransportAircraft aircraft4 = new TransportAircraft("reactive",
                2, 2, "Antonov An-225");
        aircraft4.setCargoVolume(1300);
        aircraft4.setCargoWeight(250000);
        aircraft4.setFuelUsage(7000);
        aircraft4.setMaxDistance(15400);
        company.addAircraft(aircraft4);

        TransportAircraft aircraft5 = new TransportAircraft("reactive",
                2, 2, "Boeing-747 LCF");
        aircraft5.setCargoVolume(1840);
        aircraft5.setCargoWeight(83325);
        aircraft5.setFuelUsage(2600);
        aircraft5.setMaxDistance(7800);
        company.addAircraft(aircraft5);

        System.out.println("Total volume of aircrafts are " + company.getSunOfVolume() + "m");
        System.out.println("Total weight of aircrafts are " + company.getSumOfWeight() + "kg");
        System.out.println("Aircrafts, sorted by max distance are: ");
        printAircraftsInOrder(company.sortByDistance(company.getAircraftList()));

        System.out.println("Enter minimum fuel usage: ");
        int minFuelUse = insertNumber();
        System.out.println("Enter maximum fuel usage: ");
        int maxFuelUse = insertNumber();

        System.out.println("Aircrafts with nminimum usage" + minFuelUse + " and maximum usage "
            + maxFuelUse + " are: ");

        Object [] crafts = company.filterByFuelUse(minFuelUse,maxFuelUse);
        if (crafts.length==0){
            System.out.println("No sach aircrafts");
        }
        else {
            for (Object n: crafts){
                System.out.println(n);
            }
        }

    }

    public static void printAircraftsInOrder (ArrayList<TransportAircraft> aircrafts){
        for (TransportAircraft n: aircrafts) {
            System.out.println(n);
        }
    }

    private static int insertNumber() {
        Scanner input = new Scanner(System.in);
        boolean continueInput = true;
        int value = 0;
        do {
            try{
                value = input.nextInt();
                continueInput = false;
                System.out.println(
                        "The number entered is " + value);
            }
            catch (InputMismatchException ex) {
                System.out.println("Try again. (" +
                        "Incorrect input: an integer is required)");
                input.nextLine();
            }
        }
        while (continueInput);

        return value;
    }
}
