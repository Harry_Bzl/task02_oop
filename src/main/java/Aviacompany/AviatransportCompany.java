package Aviacompany;

import java.util.ArrayList;
import java.util.Comparator;

public class AviatransportCompany {
    private ArrayList <TransportAircraft> aircraftList;

    public AviatransportCompany() {
        aircraftList = new ArrayList<TransportAircraft>();
    }

    public ArrayList<TransportAircraft> getAircraftList() {
        return aircraftList;
    }

    public void addAircraft (TransportAircraft airCraft){
        aircraftList.add(airCraft);
    }

    public boolean removeAircraft (TransportAircraft aircraft){
        return aircraftList.remove(aircraft);
    }

    public int getSunOfVolume (){
        int totalVolume = 0;
        for (TransportAircraft craft: aircraftList){
            totalVolume+=craft.getCargoVolume();
        }
        return totalVolume;
    }

    public int getSumOfWeight () {
        int totalWeight = 0;
        for (TransportAircraft craft: aircraftList){
            totalWeight+=craft.getCargoWeight();
        }
        return totalWeight;
    }

    public ArrayList<TransportAircraft> sortByDistance (ArrayList <TransportAircraft> aircrafts){
        ArrayList<TransportAircraft> newList = aircrafts;
        newList.sort(Comparator.comparing(TransportAircraft::getMaxDistance));
        return newList;
    }

    public Object[] filterByFuelUse (int minUse, int maxUse) {
        Object [] result;
        result = aircraftList.stream().filter((i)-> minUse<i.getFuelUsage()&&
                i.getFuelUsage()<maxUse).toArray();
        return result;
    }
}
