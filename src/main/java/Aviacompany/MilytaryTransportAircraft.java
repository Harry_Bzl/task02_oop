package Aviacompany;

public class MilytaryTransportAircraft extends TransportAircraft implements Maneverrable {
    public MilytaryTransportAircraft(String engineType, int engineQuantity, int pilotQuantity, String model) {
        super(engineType, engineQuantity, pilotQuantity, model);
    }

    public void maneuver() {
        System.out.println("Aircraft can maneuver.");
    }
}
